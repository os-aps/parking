# About

This repo is meant to collect use cases for typesetting with Pagedjs that currently do not work or require workarounds.
Most use cases will be in test.html (and comments in the code), but more can be extracted into 1_,2_... files for minimum reproduction.